# =========================================================================================
# Copyright (C) 2021 Orange & contributors
#
# This program is free software; you can redistribute it and/or modify it under the terms
# of the GNU Lesser General Public License as published by the Free Software Foundation;
# either version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License along with this
# program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
# Floor, Boston, MA  02110-1301, USA.
# =========================================================================================
# default workflow rules: Merge Request pipelines
workflow:
  rules:
    # prevent branch pipeline when an MR is open (prefer MR pipeline)
    - if: '$CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS'
      when: never
    - when: always

# test job prototype: implement adaptive pipeline rules
.test-policy:
  rules:
    # on tag: auto & failing
    - if: $CI_COMMIT_TAG
    # on ADAPTIVE_PIPELINE_DISABLED: auto & failing
    - if: '$ADAPTIVE_PIPELINE_DISABLED == "true"'
    # on production or integration branch(es): auto & failing
    - if: '$CI_COMMIT_REF_NAME =~ $PROD_REF || $CI_COMMIT_REF_NAME =~ $INTEG_REF'
    # early stage (dev branch, no MR): manual & non-failing
    - if: '$CI_MERGE_REQUEST_ID == null && $CI_OPEN_MERGE_REQUESTS == null'
      when: manual
      allow_failure: true
    # Draft MR: auto & non-failing
    - if: '$CI_MERGE_REQUEST_TITLE =~ /^Draft:.*/'
      allow_failure: true
    # else (Ready MR): auto & failing
    - when: on_success

variables:
  # variabilized tracking image
  TBC_TRACKING_IMAGE: "$CI_REGISTRY/to-be-continuous/tools/tracking:master"

  # Default Node project root directory
  NODE_PROJECT_DIR: .
  # Default node source directory
  NODE_SOURCE_DIR: "src"
  # Default node build directory
  NODE_BUILD_DIR: "dist"
  # Default docker image for Node
  NODE_IMAGE : "registry.hub.docker.com/library/node:lts-alpine"
  # Node lint
  NODE_LINT_ARGS: "run lint"
  # Node build
  NODE_BUILD_ARGS: "run build --prod"
  # Node test
  NODE_TEST_ARGS: "test -- --coverage"

  # Audit
  NODE_AUDIT_LEVEL: "low"
  NODE_AUDIT_ARGS: "--audit-level=$NODE_AUDIT_LEVEL"

  # Outdated
  NODE_OUTDATED_ARGS: "--long"

  NODE_SBOM_OPTS: "--omit dev"

  # default production ref name (pattern)
  PROD_REF: '/^(master|main)$/'
  # default integration ref name (pattern)
  INTEG_REF: '/^develop$/'

# ==================================================
# Stages definition
# ==================================================
stages:
  - build
  - test

.node-scripts: &node-scripts |
  # BEGSCRIPT
  set -e

  function log_info() {
      echo -e "[\\e[1;94mINFO\\e[0m] $*"
  }

  function log_warn() {
      echo -e "[\\e[1;93mWARN\\e[0m] $*"
  }

  function log_error() {
      echo -e "[\\e[1;91mERROR\\e[0m] $*"
  }

  function assert_defined() {
    if [[ -z "$1" ]]
    then
      log_error "$2"
      exit 1
    fi
  }

  function install_ca_certs() {
    certs=$1
    if [[ -z "$certs" ]]
    then
      return
    fi

    # import in system
    if echo "$certs" >> /etc/ssl/certs/ca-certificates.crt
    then
      log_info "CA certificates imported in \\e[33;1m/etc/ssl/certs/ca-certificates.crt\\e[0m"
    fi
    if echo "$certs" >> /etc/ssl/cert.pem
    then
      log_info "CA certificates imported in \\e[33;1m/etc/ssl/cert.pem\\e[0m"
    fi

    # configure for npm
    echo "$certs" > /tmp/custom-ca.pem
    export NODE_EXTRA_CA_CERTS=/tmp/custom-ca.pem

    # import in Java keystore (if keytool command found)
    if command -v keytool > /dev/null
    then
      # shellcheck disable=SC2046
      javahome=${JAVA_HOME:-$(dirname $(readlink -f $(command -v java)))/..}
      # shellcheck disable=SC2086
      keystore=${JAVA_KEYSTORE_PATH:-$(ls -1 $javahome/jre/lib/security/cacerts 2>/dev/null || ls -1 $javahome/lib/security/cacerts 2>/dev/null || echo "")}
      if [[ -f "$keystore" ]]
      then
        storepass=${JAVA_KEYSTORE_PASSWORD:-changeit}
        nb_certs=$(echo "$certs" | grep -c 'END CERTIFICATE')
        log_info "importing $nb_certs certificates in Java keystore \\e[33;1m$keystore\\e[0m..."
        for idx in $(seq 0 $((nb_certs - 1)))
        do
          # TODO: use keytool option -trustcacerts ?
          if echo "$certs" | awk "n==$idx { print }; /END CERTIFICATE/ { n++ }" | keytool -noprompt -import -alias "imported CA Cert $idx" -keystore "$keystore" -storepass "$storepass"
          then
            log_info "... CA certificate [$idx] successfully imported"
          else
            log_warn "... Failed importing CA certificate [$idx]: abort"
            return
          fi
        done
      else
        log_warn "Java keystore \\e[33;1m$keystore\\e[0m not found: could not import CA certificates"
      fi
    fi
  }

  function unscope_variables() {
    _scoped_vars=$(env | awk -F '=' "/^scoped__[a-zA-Z0-9_]+=/ {print \$1}" | sort)
    if [[ -z "$_scoped_vars" ]]; then return; fi
    log_info "Processing scoped variables..."
    for _scoped_var in $_scoped_vars
    do
      _fields=${_scoped_var//__/:}
      _condition=$(echo "$_fields" | cut -d: -f3)
      case "$_condition" in
      if) _not="";;
      ifnot) _not=1;;
      *)
        log_warn "... unrecognized condition \\e[1;91m$_condition\\e[0m in \\e[33;1m${_scoped_var}\\e[0m"
        continue
      ;;
      esac
      _target_var=$(echo "$_fields" | cut -d: -f2)
      _cond_var=$(echo "$_fields" | cut -d: -f4)
      _cond_val=$(eval echo "\$${_cond_var}")
      _test_op=$(echo "$_fields" | cut -d: -f5)
      case "$_test_op" in
      defined)
        if [[ -z "$_not" ]] && [[ -z "$_cond_val" ]]; then continue;
        elif [[ "$_not" ]] && [[ "$_cond_val" ]]; then continue;
        fi
        ;;
      equals|startswith|endswith|contains|in|equals_ic|startswith_ic|endswith_ic|contains_ic|in_ic)
        # comparison operator
        # sluggify actual value
        _cond_val=$(echo "$_cond_val" | tr '[:punct:]' '_')
        # retrieve comparison value
        _cmp_val_prefix="scoped__${_target_var}__${_condition}__${_cond_var}__${_test_op}__"
        _cmp_val=${_scoped_var#"$_cmp_val_prefix"}
        # manage 'ignore case'
        if [[ "$_test_op" == *_ic ]]
        then
          # lowercase everything
          _cond_val=$(echo "$_cond_val" | tr '[:upper:]' '[:lower:]')
          _cmp_val=$(echo "$_cmp_val" | tr '[:upper:]' '[:lower:]')
        fi
        case "$_test_op" in
        equals*)
          if [[ -z "$_not" ]] && [[ "$_cond_val" != "$_cmp_val" ]]; then continue;
          elif [[ "$_not" ]] && [[ "$_cond_val" == "$_cmp_val" ]]; then continue;
          fi
          ;;
        startswith*)
          if [[ -z "$_not" ]] && [[ "$_cond_val" != "$_cmp_val"* ]]; then continue;
          elif [[ "$_not" ]] && [[ "$_cond_val" == "$_cmp_val"* ]]; then continue;
          fi
          ;;
        endswith*)
          if [[ -z "$_not" ]] && [[ "$_cond_val" != *"$_cmp_val" ]]; then continue;
          elif [[ "$_not" ]] && [[ "$_cond_val" == *"$_cmp_val" ]]; then continue;
          fi
          ;;
        contains*)
          if [[ -z "$_not" ]] && [[ "$_cond_val" != *"$_cmp_val"* ]]; then continue;
          elif [[ "$_not" ]] && [[ "$_cond_val" == *"$_cmp_val"* ]]; then continue;
          fi
          ;;
        in*)
          if [[ -z "$_not" ]] && [[ "__${_cmp_val}__" != *"__${_cond_val}__"* ]]; then continue;
          elif [[ "$_not" ]] && [[ "__${_cmp_val}__" == *"__${_cond_val}__"* ]]; then continue;
          fi
          ;;
        esac
        ;;
      *)
        log_warn "... unrecognized test operator \\e[1;91m${_test_op}\\e[0m in \\e[33;1m${_scoped_var}\\e[0m"
        continue
        ;;
      esac
      # matches
      _val=$(eval echo "\$${_target_var}")
      log_info "... apply \\e[32m${_target_var}\\e[0m from \\e[32m\$${_scoped_var}\\e[0m${_val:+ (\\e[33;1moverwrite\\e[0m)}"
      _val=$(eval echo "\$${_scoped_var}")
      export "${_target_var}"="${_val}"
    done
    log_info "... done"
  }

  function guess_node_manager_system() {
    case "${NODE_MANAGER:-auto}" in
    auto)
      ;;
    npm)
      log_info "--- Build system explictly declared: npm"
      return
      ;;
    yarn)
      log_info "--- Build system explictly declared: yarn"
      return
      ;;
    *)
      log_warn "--- Unknown declared node manager system: \\e[33;1m${NODE_MANAGER}\\e[0m: please read template doc"
      ;;
    esac

    if [[ -f "package-lock.json" ]]
    then
      log_info "--- Build system auto-detected: npm"
      export NODE_MANAGER="npm"
    elif [[ -f "yarn.lock" ]]
    then
      log_info "--- Build system auto-detected: yarn"
      export NODE_MANAGER="yarn"
    else
      log_error "--- Node manager system auto-detect failed: please read template doc"
      exit 1
    fi
  }

  function sonar_lint_report() {
    if [[ "$SONAR_HOST_URL" ]] || [[ "$SONAR_URL" ]]
    then
      mkdir -p -m 777 reports
      # generate eslint report in json for SonarQube
      # shellcheck disable=SC2086
      $NODE_MANAGER $NODE_LINT_ARGS -- --format=json --output-file=reports/node-lint.xslint.json
    fi
  }

  unscope_variables

  # ENDSCRIPT

.node-base:
  image: $NODE_IMAGE
  services:
    - name: "$TBC_TRACKING_IMAGE"
      command: ["--service", "node", "3.5.3" ]
  variables:
    # Yarn cache (better than --cache-folder option, deprecated)
    YARN_CACHE_FOLDER: "$CI_PROJECT_DIR/$NODE_PROJECT_DIR/.yarn"
    # NPM cache
    npm_config_cache: "$CI_PROJECT_DIR/$NODE_PROJECT_DIR/.npm"
  # Cache downloaded dependencies and plugins between builds.
  # To keep cache across branches add 'key: "$CI_JOB_NAME"'
  cache:
    key: "$CI_COMMIT_REF_SLUG-node"
    paths:
      - $NODE_PROJECT_DIR/.npm/
      - $NODE_PROJECT_DIR/.yarn/
  before_script:
    - *node-scripts
    - install_ca_certs "${CUSTOM_CA_CERTS:-$DEFAULT_CA_CERTS}"
    - cd ${NODE_PROJECT_DIR}
    - guess_node_manager_system
    - config_registry=${NODE_CONFIG_REGISTRY:-$NPM_CONFIG_REGISTRY}
    - if [[ "$config_registry" ]]; then $NODE_MANAGER config set registry $config_registry; fi
    - |
      case "$NODE_MANAGER" in
      npm)
        npm ci --prefer-offline $NODE_INSTALL_EXTRA_OPTS
        ;;
      yarn)
        yarn install --immutable --immutable-cache --check-cache $NODE_INSTALL_EXTRA_OPTS
      ;;
      esac

node-build:
  extends: .node-base
  stage: build
  script:
    # launch unit test and code coverage
    - $NODE_MANAGER $NODE_TEST_ARGS
    - if [[ "$NODE_BUILD_DISABLED" != "true" ]]; then $NODE_MANAGER $NODE_BUILD_ARGS; fi
  coverage: '/^All files\s*\|\s*(\d+(?:\.\d+)?)/'
  artifacts:
    reports:
      coverage_report:
        coverage_format: cobertura
        path: "$NODE_PROJECT_DIR/reports/cobertura-coverage.xml"
      junit:
        - $NODE_PROJECT_DIR/reports/node-test.xunit.xml
    when: always
    name: "$CI_JOB_NAME artifacts from $CI_PROJECT_NAME on $CI_COMMIT_REF_SLUG"
    paths:
      - $NODE_PROJECT_DIR/$NODE_BUILD_DIR
      - $NODE_PROJECT_DIR/reports/node-test.*
      - $NODE_PROJECT_DIR/reports/node-coverage.*
      # no way to override default coverage reports when using Mocha
      - $NODE_PROJECT_DIR/reports/lcov.info
      - $NODE_PROJECT_DIR/reports/cobertura-coverage.xml
    expire_in: 1 day
  rules:
    # always if $NODE_BUILD_DISABLED not set
    - if: '$NODE_BUILD_DISABLED != "true"'
    - !reference [.test-policy, rules]

node-lint:
  extends: .node-base
  stage: build
  script:
    # generate lint report for sonar
    - sonar_lint_report
    # display lint result for console
    - $NODE_MANAGER $NODE_LINT_ARGS
  artifacts:
    when: always # store artifact even if test Failed
    name: "$CI_JOB_NAME artifacts from $CI_PROJECT_NAME on $CI_COMMIT_REF_SLUG"
    paths:
      - $NODE_PROJECT_DIR/reports/node-lint.xslint.json
    expire_in: 1 day
  rules:
    # exclude if $NODE_LINT_ENABLED unset
    - if: '$NODE_LINT_ENABLED != "true"'
      when: never
    # on production or integration branch(es): auto & failing
    - !reference [.test-policy, rules]

###############################################################################################
# Test stage: audit & outdated
###############################################################################################

# audit
node-audit:
  extends: .node-base
  stage: test
  needs: []
  script:
    # JSON (for DefectDojo)
    - |
      if [[ "$DEFECTDOJO_NPMAUDIT_REPORTS" ]]
      then
        mkdir -p -m 777 reports
        $NODE_MANAGER audit --json $NODE_AUDIT_ARGS > reports/npm-audit.native.json || true
      fi
    # last run with console output
    - $NODE_MANAGER audit $NODE_AUDIT_ARGS
  artifacts:
    when: always
    name: "$CI_JOB_NAME artifacts from $CI_PROJECT_NAME on $CI_COMMIT_REF_SLUG"
    paths:
      - $NODE_PROJECT_DIR/reports/npm-audit.*
    expire_in: 1 day
  rules:
    # exclude if $NODE_AUDIT_DISABLED set
    - if: '$NODE_AUDIT_DISABLED == "true"'
      when: never
    - !reference [.test-policy, rules]

# outdated
node-outdated:
  extends: .node-base
  stage: test
  script:
    - |
      if [[ "$DEFECTDOJO_NPMAUDIT_REPORTS" ]]
      then
        mkdir -p -m 777 reports
        $NODE_MANAGER outdated --json $NODE_OUTDATED_ARGS > reports/npm-outdated.native.json || true
      fi
    - $NODE_MANAGER outdated $NODE_OUTDATED_ARGS
  artifacts:
    when: always
    name: "$CI_JOB_NAME artifacts from $CI_PROJECT_NAME on $CI_COMMIT_REF_SLUG"
    paths:
      - $NODE_PROJECT_DIR/reports/npm-outdated.*
    expire_in: 1 day
  rules:
    # exclude if $NODE_OUTDATED_DISABLED set
    - if: $NODE_OUTDATED_DISABLED == "true"
      when: never
    # on production or integration branch(es): auto & non-blocking
    - if: '$CI_COMMIT_REF_NAME =~ $PROD_REF || $CI_COMMIT_REF_NAME =~ $INTEG_REF'
      allow_failure: true
    # on non-production, non-integration branches: manual & non-blocking
    - when: manual
      allow_failure: true

node-sbom:
  extends: .node-base
  stage: test
  # force no dependency
  dependencies: []
  script:
    - mkdir -p -m 777 reports
    - npx -y @cyclonedx/cyclonedx-npm${NODE_SBOM_VERSION:+@$NODE_SBOM_VERSION} --output-format JSON --output-file reports/node-sbom.cyclonedx.json $NODE_SBOM_OPTS
    - chmod a+r reports/node-sbom.cyclonedx.json
  artifacts:
    name: "SBOM for Node from $CI_PROJECT_NAME on $CI_COMMIT_REF_SLUG"
    when: always
    expire_in: 1 week
    paths:
      - $NODE_PROJECT_DIR/reports/node-sbom.cyclonedx.json
    reports:
      cyclonedx: 
        - $NODE_PROJECT_DIR/reports/node-sbom.cyclonedx.json
  rules:
    # exclude if disabled
    - if: '$NODE_SBOM_DISABLED == "true"'
      when: never
    - !reference [.test-policy, rules]
